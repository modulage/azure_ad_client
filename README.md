Find detailed instructions here: https://vizito.eu/faq/ldap/windows-azure/

### Troubleshooting 

* Demo services are available for testing purposes. They include names such as "Tesla", "Newton", etc..

    * HTTP version: http://178.162.136.223:48800
    * HTTPS version: https://source.vizito.eu:48801
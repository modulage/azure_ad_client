var fs = require('fs');
var http = require("http");
var https = require('https');
var scheduler = require('node-schedule');
var config = require('./config.json');
require('isomorphic-fetch');
var MicrosoftGraph = require("@microsoft/microsoft-graph-client").Client;
var request = require("request");
const url = require('url');

var cachedResults = [];
var domainsArray = [];
var options;

config.domains.forEach(function (configuration) {
    options = {
        method: 'POST',
        url: 'https://login.microsoftonline.com/' + configuration.tenant_id + '/oauth2/v2.0/token',
        headers:
            {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
        form:
            {
                client_id: configuration.client_id,
                scope: 'https://graph.microsoft.com/.default',
                client_secret: configuration.client_secret,
                grant_type: 'client_credentials'
            }
    };
    configuration.options = options;
    domainsArray.push(configuration);
});

function getPhoneWithPrefix(number, mobile_prefix) {
    if (number && mobile_prefix) {
        if (number.substring(0, 2) == "00" || number.substring(0, 1) == "+") {
            return number;
        }
        if (number.substring(0, 1) == "0") {
            number = mobile_prefix + number.substring(1);
        } else {
            number = mobile_prefix + number;
        }
    }
    return number;
}

function runserver(request, response) {

    response.writeHead(200, {
        "Content-Type": "application/json; charset=utf-8",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "X-Requested-With, Content-Type, Authorization, Language"
    });

    response.write(JSON.stringify(cachedResults));
    response.end();

};

var hourlyRule = new scheduler.RecurrenceRule();
hourlyRule.minute = 0;

scheduler.scheduleJob(hourlyRule, fetchData);

var adUsers;

function fetchData() {
    adUsers = [];
    cachedResults = [];
    var i = 0;

    domainsArray.forEach(function (configuration) {
        requestCall(configuration);
    });
};

function requestCall(configuration) {
    request(configuration.options, function (error, response, body) {
        if (error) throw new Error(error);
        access_token = JSON.parse(body).access_token;
        var client = MicrosoftGraph.init({
            defaultVersion: 'v1.0',
            debugLogging: true,
            authProvider: (done) => {
                done(null, access_token);
            }
        });
        fetchChunk(client, null, configuration);
    });
};

function fetchChunk(client, skipToken, configuration) {
    var apiMethod = configuration.group ? '/groups/'+configuration.group+'/members' : '/users';
    var apiCall = client.api(apiMethod)
        .version('v1.0')
        .top(999);
    if (skipToken != null) {
        apiCall = apiCall.skipToken(skipToken);
    }
    apiCall.get((err, res) => {
        if(err) {
            console.log(err);
            return;
        }

        adUsers = adUsers.concat(res.value);
        if (res['@odata.nextLink'] != undefined && res['@odata.nextLink'] != '') {
            var parsedUrl = new url.URL(res['@odata.nextLink']);
            var newSkipToken = parsedUrl.searchParams.get('$skiptoken');
            if (newSkipToken == null) {
                newSkipToken = parsedUrl.searchParams.get('$skipToken');
            }
            if (newSkipToken == null) {
                setTimeout(parseUsers, 0 , configuration);
            } else {
                fetchChunk(client, newSkipToken, configuration);
            }
        } else {
            setTimeout(parseUsers, 0, configuration);
        }
    });
}

function parseUsers(configuration) {
    var results = [];
    if (adUsers === undefined) {
        return;
    }
    adUsers.forEach(function (adUser) {
        if (adUser.givenName == null && adUser.surname == null) return;

        user = {cn: adUser.givenName + " " + adUser.surname, mail: adUser.mail};
        if (configuration.exclusions.indexOf(user.mail) >= 0) return;
        if (adUser.mobilePhone != undefined) {
            user["mobile"] = getPhoneWithPrefix(adUser.mobilePhone, configuration.mobile_prefix);
        }
        if (adUser.businessPhones.length > 0) {
            user["telephoneNumber"] = getPhoneWithPrefix(adUser.businessPhones[0], configuration.mobile_prefix);
        }
        results.push(user);
    });
    cachedResults.push(...results);
}

fetchData();

var server;

if (config.use_ssl_for_api) {
    var caArray = [];
    if (config.ssl.ca && config.ssl.ca.length > 0) {
        config.ssl.ca.forEach(function (caReference) {
            caArray.push(fs.readFileSync(caReference));
        });
    }

    server = https.createServer({
        ca: caArray,
        key: fs.readFileSync(config.ssl.key),
        cert: fs.readFileSync(config.ssl.cert),
        ciphers: [
            "ECDHE-RSA-AES256-SHA384",
            "DHE-RSA-AES256-SHA384",
            "ECDHE-RSA-AES256-SHA256",
            "DHE-RSA-AES256-SHA256",
            "ECDHE-RSA-AES128-SHA256",
            "DHE-RSA-AES128-SHA256",
            "HIGH",
            "!aNULL",
            "!eNULL",
            "!EXPORT",
            "!DES",
            "!RC4",
            "!MD5",
            "!PSK",
            "!SRP",
            "!CAMELLIA"
        ].join(':')
    }, function (request, response) {
        runserver(request, response);
    });
} else {
    server = http.createServer(function (request, response) {
        runserver(request, response);
    });
}

server.listen(config.api_port);

console.log((config.use_ssl_for_api ? "HTTPS" : "HTTP") + " server is listening on port " + config.api_port);
